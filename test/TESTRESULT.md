

**Version**:  

**Date**: 

**Performed by**: 

**Test setup**: 

IOC Test (serial version)
======== 

The following tests are conducted using manual CA commands from promt. This is the test for the serial version of the protocol (no LAN module, connection through Moxa box).

| Test case            | Result | Comment                                                                 | 
| -------------------- | ------ | ----------------------------------------------------------------------- | 
| Basic connection     |  |              | 
| CAGet on Vol-R       |  |              | 
| CAGet on Cur-R       |  |              | 
| CAGet on Idn-R       |  |              | 
| CAGet on ISW-R     |  |              | 
| CAGet on SN-R    |  |              | 
| CAGet on DATE-R  |  |              | 
| CAGet on Rmt-RB |  |             | 
| CAGet on MDAV-RB  |  |      | 
| CAPut Vol-S to 10V  |  |   | 
| CAGet on Vol-RB       |  |              | 
| CAGet on Vol-R       |  |              | 
| CAPut Cur-S to 1 A |  |  | 
| CAGet on Cur-RB       |  |              | 
| CAGet on Cur-R       |  |              | 
| CAPut Rst to 1 | | | 
| CAPut Out-S to OFF |  |  | 
| CAGet Out-RB |  |  | 
| CAPut Out-S to ON |  |  | 
| CAPut FLD-S to ON | |  | 
| CAPut FBD-S to 105 |  |  | 
| CAGet FBD-RB |  |  |
| CAPut FBDRst to 1 |  |  | 
| CAGet FBD-RB |  |  |
| CAPut OVP-S to 9V | |  |
| CAGet OVP-RB |  |  |
| CAPut OVM-S to 11V | |  |
| CAGet OVP-RB |  |  |
| CAPut UVL-S to 3V | |  |
| CAGet UVL-RB |  |  |
| CAPut AST-S to ON | |  |
| CAGet AST-RB |  |  |
| CAPut Rst to 1 | | | 

IOC Test (LAN version)
======== 

The following tests are conducted using manual CA commands from promt. This test is for the LAN version of the protocol.

| Test case            | Result | Comment                                                                 | 
| -------------------- | ------ | ----------------------------------------------------------------------- | 
| Basic connection     |  |              | 
| CAGet on Vol-R       |  |              | 
| CAGet on Cur-R       |  |              | 
| CAGet on Idn-R       |  |              | 
| CAGet on ISW-R     |  |              | 
| CAGet on SN-R    |  |              | 
| CAGet on Rmt-RB |  |             | 
| CAPut Vol-S to 10V  |  |   | 
| CAGet on Vol-RB       |  |              | 
| CAGet on Vol-R       |  |              | 
| CAPut Cur-S to 1 A |  |  | 
| CAGet on Cur-RB       |  |              | 
| CAGet on Cur-R       |  |              | 
| CAPut Rst to 1 | | | 
| CAPut Out-S to OFF |  |  | 
| CAGet Out-RB |  |  | 
| CAPut Out-S to ON |  |  | 
| CAPut FLD-S to ON | |  | 
| CAPut OVP-S to 9V | |  |
| CAGet OVP-RB |  |  |
| CAPut OVM-S to 11V | |  |
| CAGet OVP-RB |  |  |
| CAPut UVL-S to 3V | |  |
| CAGet UVL-RB |  |  |
| CAPut AST-S to ON | |  |
| CAGet AST-RB |  |  |
| CAPut Rst to 1 | | | 
             



