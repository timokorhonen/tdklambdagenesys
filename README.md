tdklambdagenesys
======
European Spallation Source ERIC Site-specific EPICS module : tdklambdagenesys

This module provides device support for the TDK Lambda Genesys family of programmable DC power supplies. The device support is based on the LAN interface, and can therefore only be used for power supplies that has the optional LAN module.

- Manufacturer page: https://www.us.lambda.tdk.com/products/programmable-power/genesys.html
- User manual: /docs/GEN5000W User Manual IA657-04-01M.pdf
- Data sheet: /docs/93530001K2.pdf
- LAN Interface USer Manual: /docs/Genesys Power Supplies LAN Interface (83034100).pdf

Dependencies
============
stream


Using the module
================
In order to use the device support module declare a require statement for streamdevice and tdklambdagenesys:

    require stream
    require tdklambdagenesys

Then load the module:

    iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-01, IP_ADDR=10.31.1.1, P=ISrc-010:, R=PwrC-CoilPS-01:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=33")

Parameters:
- CONNECTIONNAME: The name of the asyn IP port connection. For trouble shooting purposes.
- IP_ADDR: The IP address of your power supply LAN module
- P: The section and subsection part of your device name (include the colon)
- R: The discipline, device and index part of your device name (include the colon)
- MAXVOL: The maximum voltage of your power supply (e.g. 10V)
- MAXCUR: The maximum current of your power supply (e.g. 500A)
- HICUR: The limit at wich a HI alarm of MINOR severity will be activated for the measured current
- HIHICUR: The limit at wich a HIHI alarm of MAJOR severity will be activated for the measured current
- MAXOVP: The maximum level the over voltage protection limit can be set at for your power supply
- DEVICETYPE (optional): The type of device used. Defined values: tdklambdagenesysLAN, tdklambdagenesys_serial (default=tdklambdagenesys_serial)
- LANMODULE (optional): Set to one for devices using the optional LAN module. This will disable PV's that are not supported on the LAN modules (default=0)


Building the module
===================
This module should compile as a regular EPICS module.

* Run `make` from the command line.

This module should also compile as an E3 module:

* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require`
* Run `make -f Makefile.E3 target` to build `target` using the E3 build process
